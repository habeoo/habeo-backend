FROM amazoncorretto:17-alpine3.14

COPY build/libs/app.jar app.jar

ENTRYPOINT exec java -jar app.jar

