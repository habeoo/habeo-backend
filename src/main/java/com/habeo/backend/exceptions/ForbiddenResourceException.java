package com.habeo.backend.exceptions;

import org.springframework.http.HttpStatus;

public class ForbiddenResourceException extends BusinessException {
    public ForbiddenResourceException(String message) {
        super(HttpStatus.FORBIDDEN, message);
    }
}
