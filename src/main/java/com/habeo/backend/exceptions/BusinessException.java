package com.habeo.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class BusinessException extends HttpStatusCodeException {
    public BusinessException(HttpStatus status, String message) {
        super(status, message);
    }
}
