package com.habeo.backend.groups.models.dtos;

import com.habeo.backend.oauth2.models.User;

import javax.validation.constraints.Pattern;

public record CreateGroupDto(
        @Pattern(regexp = "^[a-zA-Z,.\s]+$",
                message = "Only letters, comma and dot allowed")
        String name,
        @Pattern(regexp = "^[a-zA-Z,.\s]+$",
                message = "Only letters, comma and dot allowed")
        String description) {
}
