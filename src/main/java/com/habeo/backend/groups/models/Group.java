package com.habeo.backend.groups.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.oauth2.models.User;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;


@Entity
@Table(name = "groups")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String invitationUrl;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_to_group",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    Set<User> members;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "group_to_habit",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "habit_id", referencedColumnName = "id")
    )
    Set<Habit> habits;
}
