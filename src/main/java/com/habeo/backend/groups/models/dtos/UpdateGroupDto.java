package com.habeo.backend.groups.models.dtos;

import javax.validation.constraints.Pattern;

public record UpdateGroupDto(
        @Pattern(regexp = "^[a-zA-Z,.\s]+$",
                message = "Only letters, comma and dot allowed")
        String name,
        @Pattern(regexp = "^[a-zA-Z,.\s]+$",
                message = "Only letters, comma and dot allowed")
        String description
) {
}
