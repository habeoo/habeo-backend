package com.habeo.backend.groups.repositories;

import com.habeo.backend.groups.models.Group;
import com.habeo.backend.oauth2.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findGroupById(Long id);

    Group findGroupByIdAndMembersContaining(Long id, User user);

    Group findGroupByInvitationUrl(String url);

    List<Group> findAllByMembersContaining(User user);

    void deleteGroupByIdAndMembersContaining(Long id, User user);
}
