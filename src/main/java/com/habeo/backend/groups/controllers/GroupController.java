package com.habeo.backend.groups.controllers;

import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.models.dtos.CreateGroupDto;
import com.habeo.backend.groups.models.dtos.UpdateGroupDto;
import com.habeo.backend.groups.services.GroupCompetitionService;
import com.habeo.backend.groups.services.GroupService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.services.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/groups")
public class GroupController {
    private final TagService tagService;

    private final GroupService groupService;

    private final GroupCompetitionService competitionService;

    @PostMapping
    public ResponseEntity<Group> createNewGroup(@Validated @RequestBody CreateGroupDto groupDto, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Group group = groupService.createNewGroup(groupDto, user);
        return ResponseEntity.created(URI.create("/habits/" + group.getId())).body(group);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Group> getGroup(@PathVariable Long id, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Group group = groupService.getGroup(id, user);
        return ResponseEntity.ok(group);
    }

    @PostMapping("/{groupUrl}")
    public ResponseEntity<Group> addMemberToGroup(@PathVariable String groupUrl, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Group group = groupService.getGroup(groupUrl);
        groupService.addMemberToGroup(group.getId(), user);
        return ResponseEntity.ok(group);
    }

    @GetMapping("/{id}/ranking")
    public ResponseEntity<List<User>> getMembersRanking(@PathVariable Long id, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Group group = groupService.getGroup(id, user);
        LocalDate now = LocalDate.now();
        Map<User, Integer> groupConsistency = competitionService.getGroupConsistencyRecord(group.getId(), now);
        return ResponseEntity.ok(competitionService.sortMembersBasedOnConsistencyRecord(groupConsistency));
    }

    @GetMapping
    public ResponseEntity<List<Group>> getUserGroups(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        List<Group> groups = groupService.getUserGroups(user);
        return ResponseEntity.ok(groups);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Group> editGroup(@Validated @RequestBody UpdateGroupDto groupDto, @PathVariable Long id, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Group group = groupService.updateGroup(groupDto, id, user);
        return ResponseEntity.ok(group);
    }

    @PostMapping("/{id}/habits")
    public ResponseEntity<Group> editGroupHabits(@PathVariable Long id, @RequestBody HabitDto habitDto, Principal principal) throws IOException {
        User user = tagService.getUserFromPrincipal(principal);
        if (!groupService.userBelongsToGroup(id, user)) {
            throw new ForbiddenResourceException("User not in group");
        }
        Habit habit = groupService.addHabitToGroup(habitDto, id);
        groupService.generateHabitEventsForMembers(id, habit, habitDto.getCron());
        Group group = groupService.getGroup(id, user);
        return ResponseEntity.ok(group);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGroup(@PathVariable Long id, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        groupService.deleteGroup(id, user);
        return ResponseEntity.ok().build();
    }
}
