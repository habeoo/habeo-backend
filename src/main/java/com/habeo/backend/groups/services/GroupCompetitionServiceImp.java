package com.habeo.backend.groups.services;

import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.repositories.GroupRepository;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.services.HabitEventService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import com.habeo.backend.oauth2.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupCompetitionServiceImp implements GroupCompetitionService {
    private final GroupRepository groupRepository;

    private final HabitEventService habitEventService;

    private final UserHabitsService userHabitsService;

//    @Override
//    @Scheduled(cron = "0 0 0 * * 1")
//    public void assignAchievement() {
//        List<Group> groups = groupRepository.findAll();
//        LocalDate now = LocalDate.now();
//        OffsetDateTime mondayOffset = getDayOfTheWeek(now, DayOfWeek.MONDAY, 0, 0);
//        OffsetDateTime sundayOffset = getDayOfTheWeek(now, DayOfWeek.MONDAY, 1, 1);
//        groups.forEach(group -> {
//            Map<User, Integer> groupConsistency = getGroupConsistencyRecord(group.getId(), mondayOffset, sundayOffset);
//            List<User> ranking = sortMembersBasedOnConsistencyRecord(groupConsistency);
//
//        });
//    }

    @Override
    public List<User> sortMembersBasedOnConsistencyRecord(Map<User, Integer> consistency) {
//        return consistency.entrySet()
//                .stream()
//                .sorted(Map.Entry.comparingByValue())
//                .map(Map.Entry::getKey)
//                .collect(Collectors.toList());
        List<Map.Entry<User, Integer>> list = new ArrayList<>(consistency.entrySet());
        list.sort(new Comparator<Map.Entry<User, Integer>>() {
            public int compare(Map.Entry<User, Integer> o1, Map.Entry<User, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        List<User> sortedKeys = new ArrayList<>();
        for (Map.Entry<User, Integer> entry : list) {
            sortedKeys.add(entry.getKey());
        }

        return sortedKeys;
    }

    @Override
    public Map<User, Integer> getGroupConsistencyRecord(Long groupId, LocalDate now) {
        OffsetDateTime mondayOffset = getDayOfTheWeek(now, DayOfWeek.MONDAY, 0, 0);
        OffsetDateTime sundayOffset = getDayOfTheWeek(now, DayOfWeek.SUNDAY, 1, 1);
        Group group = groupRepository.findGroupById(groupId);
        List<User> members = group.getMembers().stream().toList();
        List<Habit> habits = group.getHabits().stream().toList();
        Map<User, Integer> record = new HashMap<>();
        members.forEach(member -> {
            List<UserHabit> userHabitList = habits.stream()
                    .map(habit -> userHabitsService.getUserHabit(habit.getId(), member.getId())).toList();

            record.put(member, getMemberConsistencyRecord(userHabitList, mondayOffset, sundayOffset));
        });
        return record;
    }

    @Override
    public Integer getMemberConsistencyRecord(List<UserHabit> userHabit, OffsetDateTime start, OffsetDateTime end) {
        List<GeneratedHabit> events = new ArrayList<>();
        userHabit.forEach(h -> events.addAll(habitEventService.getPeriodicHabitEventsByUserHabit(h, start, end)));
        return events.size();
    }

    public OffsetDateTime getDayOfTheWeek(LocalDate now, DayOfWeek day, Integer plusDays, Integer minusSeconds) {
        LocalDate date = now.with(day);
        LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.MIDNIGHT).plusDays(plusDays).minusSeconds(minusSeconds);
        return dateTime.atOffset(ZoneOffset.UTC);
    }
}
