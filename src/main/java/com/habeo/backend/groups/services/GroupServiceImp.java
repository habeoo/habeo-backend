package com.habeo.backend.groups.services;

import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.models.dtos.CreateGroupDto;
import com.habeo.backend.groups.models.dtos.UpdateGroupDto;
import com.habeo.backend.groups.repositories.GroupRepository;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.services.GenerateHabitService;
import com.habeo.backend.habits.generated.services.HabitScheduleService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.habits.scheme.services.HabitService;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.services.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class GroupServiceImp implements GroupService {
    private final GroupRepository groupRepository;

    @Autowired
    private EntityManager entityManager;

    private final HabitService habitService;

    private final TagService tagService;

    private final UserHabitsService userHabitsService;

    private final GenerateHabitService generatedHabitService;

    private final HabitScheduleService habitScheduleService;

    @Override
    @Transactional
    public Group createNewGroup(CreateGroupDto groupDto, User user) {
        Group group = Group.builder()
                .name(groupDto.name())
                .description(groupDto.description())
                .invitationUrl(generateGroupInvitationUri())
                .habits(new HashSet<>())
                .members(Collections.singleton(user))
                .build();

        return groupRepository.save(group);
    }

    @Override
    @Transactional
    public List<Group> getUserGroups(User user) {
        List<Group> groups = groupRepository.findAllByMembersContaining(user);
        if (groups.isEmpty()) throw new NotFoundException("User's group not found");
        return groups;
    }

    @Override
    public Group getGroup(Long groupId, User user) {
        if (!userBelongsToGroup(groupId, user))
            throw new ForbiddenResourceException("You cannot access groups that you don't participate in");

        return groupRepository.findById(groupId).orElseThrow(() -> new NotFoundException("Group not found"));
    }

    @Override
    public Group getGroup(String groupUrl) {
        return groupRepository.findGroupByInvitationUrl(groupUrl);
    }

    @Override
    @Transactional
    public Habit addHabitToGroup(HabitDto habit, Long groupId) throws IOException {
        Group group = groupRepository.findGroupById(groupId);
        Habit created = habitService.createHabit(habit);
        Set<Habit> groupHabits = group.getHabits();
        groupHabits.add(created);
        group.setHabits(groupHabits);
        groupRepository.save(group);
        return created;
    }

    @Override
    @Transactional
    public Group addMemberToGroup(Long groupId, User user) {
        Group group = groupRepository.findGroupById(groupId);
        Set<User> members = group.getMembers();
        members.add(user);
        group.setMembers(members);
        generateHabitEventsForMember(groupId, user);
        return groupRepository.save(group);
    }

    @Override
    public void generateHabitEventsForMembers(Long groupId, Habit habit, String cron) {
        Group group = groupRepository.findGroupById(groupId);
        Set<User> members = group.getMembers();
        members.forEach(member -> {
            Tag defaultTag = tagService.getUsersDefaultTag(member.getId());
            UserHabit userHabit = userHabitsService.saveUserHabit(member, habit, defaultTag);
            HabitSchedule habitSchedule = habitScheduleService.createHabitSchedule(userHabit, cron);
            generatedHabitService.generateHabit(habitSchedule);
        });
    }

    @Override
    public void generateHabitEventsForMember(Long groupId, User user) {
        Group group = groupRepository.findGroupById(groupId);
        Tag defaultTag = tagService.getUsersDefaultTag(user.getId());
        Set<Habit> groupHabits = group.getHabits();
        groupHabits.forEach(h -> {
            UserHabit userHabit = userHabitsService.saveUserHabit(user, h, defaultTag);
            HabitSchedule groupSchedule = habitScheduleService.getHabitScheduleByHabitId(h.getId());
            HabitSchedule habitSchedule = habitScheduleService.createHabitSchedule(userHabit, groupSchedule.getCron());
            generatedHabitService.generateHabit(habitSchedule);
        });
    }


    @Override
    @Transactional
    public Group updateGroup(UpdateGroupDto groupDto, Long groupId, User user) {
        Group group = getGroup(groupId, user);
        group.setName(groupDto.name());
        group.setDescription(groupDto.description());

        return groupRepository.save(group);
    }

    @Override
    public void deleteGroup(Long groupId, User user) {
        groupRepository.deleteGroupByIdAndMembersContaining(groupId, user);
    }

    @Override
    public boolean userBelongsToGroup(Long groupId, User user) {
        Group group = entityManager.getReference(Group.class, groupId);
        return group.getMembers().contains(user);
    }

    private String generateGroupInvitationUri() {
        UUID uuid = UUID.randomUUID();
        return UriComponentsBuilder.fromPath(String.valueOf(uuid))
                .build()
                .toUriString();
    }
}
