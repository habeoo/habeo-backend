package com.habeo.backend.groups.services;

import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.oauth2.models.User;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

public interface GroupCompetitionService {
//    void assignAchievement();
    List<User> sortMembersBasedOnConsistencyRecord(Map<User, Integer> consistency);
    public Map<User, Integer> getGroupConsistencyRecord(Long groupId, LocalDate now);
    Integer getMemberConsistencyRecord(List<UserHabit> userHabit, OffsetDateTime start, OffsetDateTime end);
}
