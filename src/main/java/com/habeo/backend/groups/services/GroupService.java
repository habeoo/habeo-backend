package com.habeo.backend.groups.services;

import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.models.dtos.CreateGroupDto;
import com.habeo.backend.groups.models.dtos.UpdateGroupDto;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.oauth2.models.User;

import java.io.IOException;
import java.util.List;

public interface GroupService {
    Group createNewGroup(CreateGroupDto group, User user);
    List<Group> getUserGroups(User user);
    Group getGroup(Long groupId, User user);
    Group getGroup(String groupUrl);
    Group updateGroup(UpdateGroupDto groupDto, Long groupId, User user);
    void deleteGroup(Long groupId, User user);
    boolean userBelongsToGroup(Long groupId, User user);
    Habit addHabitToGroup(HabitDto habit, Long groupId) throws IOException;
    Group addMemberToGroup(Long groupId, User user);
    void generateHabitEventsForMembers(Long groupId, Habit habit, String cron);
    void generateHabitEventsForMember(Long groupId, User user);

}
