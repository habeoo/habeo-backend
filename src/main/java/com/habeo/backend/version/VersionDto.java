package com.habeo.backend.version;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Getter
@Component
public class VersionDto {
  @Value("${app.version:MissingAppVersion}")
  private String appVersion;

  @Value("${java.runtime.version:MissingJavaVersion}")
  private String javaVersion;

  @Value("${java.vm.name:MissingJavaProvider}")
  private String javaProvider;

  @Value("${app.compiledAt:MissingCompileTime}")
  private String compiledAt;

  @Value("${app.snapshotVersion:MissingSnapshotVersion}")
  private String snapshotVersion;

  private final String startedAt = OffsetDateTime.now(ZoneOffset.UTC).withNano(0).toString();
}

