package com.habeo.backend.achievements.repositories;

import com.habeo.backend.achievements.models.CompetitorAchievement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompetitorAchievementsRepository extends JpaRepository<CompetitorAchievement, Long> {
    List<CompetitorAchievement> findAllByHabit_IdAndCompetitor_User_Id(Long habitId, Long userId);

    List<CompetitorAchievement> findAllByCompetitor_User_Id(Long userId);
}
