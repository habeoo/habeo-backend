package com.habeo.backend.achievements.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.habits.scheme.models.Habit;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "competitor_to_achievement")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompetitorAchievement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Competitor competitor;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Achievement achievement;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Habit habit;
}
