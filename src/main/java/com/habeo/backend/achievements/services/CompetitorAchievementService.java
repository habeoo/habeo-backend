package com.habeo.backend.achievements.services;

import com.habeo.backend.achievements.models.Achievement;
import com.habeo.backend.achievements.models.CompetitorAchievement;
import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.habits.scheme.models.Habit;

public interface CompetitorAchievementService {
    CompetitorAchievement createCompetitorAchievement(Habit habit, Achievement achievement, Competitor competitor);
}
