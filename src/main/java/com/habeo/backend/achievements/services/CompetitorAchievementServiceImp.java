package com.habeo.backend.achievements.services;

import com.habeo.backend.achievements.models.Achievement;
import com.habeo.backend.achievements.models.CompetitorAchievement;
import com.habeo.backend.achievements.repositories.CompetitorAchievementsRepository;
import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.habits.scheme.models.Habit;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompetitorAchievementServiceImp implements CompetitorAchievementService {
    private final CompetitorAchievementsRepository competitorAchievementsRepository;

    @Override
    public CompetitorAchievement createCompetitorAchievement(Habit habit, Achievement achievement, Competitor competitor) {
        CompetitorAchievement competitorAchievement = CompetitorAchievement.builder()
                .achievement(achievement)
                .habit(habit)
                .competitor(competitor)
                .build();
        return competitorAchievementsRepository.save(competitorAchievement);
    }
}
