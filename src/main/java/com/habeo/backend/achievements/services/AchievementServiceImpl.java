package com.habeo.backend.achievements.services;

import com.habeo.backend.achievements.models.Achievement;
import com.habeo.backend.achievements.models.CompetitorAchievement;
import com.habeo.backend.achievements.repositories.AchievementRepository;
import com.habeo.backend.achievements.repositories.CompetitorAchievementsRepository;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.services.HabitEventService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AchievementServiceImpl implements AchievementService {
    private final AchievementRepository achievementRepository;

    private final CompetitorAchievementsRepository competitorAchievementsRepository;

    private final HabitEventService habitEventService;

    @Override
    public Achievement createAchievement(Long habitId, Long userId) {
        List<GeneratedHabit> habitOccurrences = habitEventService.getHabitEvents(habitId, userId).get();
        Integer occurrances;
        if (doesUserHavePastPendingHabits(habitOccurrences)) {
            occurrances = getHabitOccurrencesBeforeSkippedDay(habitOccurrences);
        }
        return null;
    }

    @Override
    public List<Achievement> getHabitAchievements(Long habitId, Long userId) {
        return competitorAchievementsRepository.findAllByHabit_IdAndCompetitor_User_Id(habitId, userId)
                .stream()
                .map(CompetitorAchievement::getAchievement)
                .collect(Collectors.toList());
    }

    @Override
    public List<Achievement> getUserAchievements(Long userId) {
        return competitorAchievementsRepository.findAllByCompetitor_User_Id(userId)
                .stream()
                .map(CompetitorAchievement::getAchievement)
                .collect(Collectors.toList());
    }

    private boolean doesUserHavePastPendingHabits(List<GeneratedHabit> habits) {
        for (GeneratedHabit habit : habits) {
            if (habit.getStatus().equals(HabitStatus.pending)) return true;
        }
        return false;
    }

    private int getHabitOccurrencesBeforeSkippedDay(List<GeneratedHabit> habits) {
        for (GeneratedHabit habit : habits) {
            if (habit.getStatus().equals(HabitStatus.pending)) {
                return habits.indexOf(habit);
            }
        }
        return habits.size();
    }
}
