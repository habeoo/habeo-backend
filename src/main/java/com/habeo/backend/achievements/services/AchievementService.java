package com.habeo.backend.achievements.services;

import com.habeo.backend.achievements.models.Achievement;

import java.util.List;

public interface AchievementService {
    Achievement createAchievement(Long habitId, Long userId);

    List<Achievement> getHabitAchievements(Long habitId, Long userId);

    List<Achievement> getUserAchievements(Long userId);
}
