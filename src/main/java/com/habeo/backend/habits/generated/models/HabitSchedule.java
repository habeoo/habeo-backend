package com.habeo.backend.habits.generated.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.habits.scheme.models.UserHabit;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "habits_schedule")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HabitSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cron;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private UserHabit userHabit;
}
