package com.habeo.backend.habits.generated.repositories;

import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.scheme.models.UserHabit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HabitScheduleRepository extends JpaRepository<HabitSchedule, Long> {
    Optional<HabitSchedule> findHabitScheduleByIdAndUserHabit_User_Id(Long scheduleId, Long userId);

    List<HabitSchedule> findAllByUserHabit_Habit_Active(Boolean active);

    Optional<HabitSchedule> findHabitScheduleByUserHabit_Habit_IdAndUserHabit_User_Id(Long habitId, Long userId);

    HabitSchedule findHabitScheduleByUserHabit_Habit_Id(Long habitId);

    List<HabitSchedule> findAllByUserHabit_User_Id(Long userId);
}
