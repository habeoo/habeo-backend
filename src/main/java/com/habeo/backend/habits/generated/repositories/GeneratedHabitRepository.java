package com.habeo.backend.habits.generated.repositories;

import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface GeneratedHabitRepository extends JpaRepository<GeneratedHabit, Long> {
    List<GeneratedHabit> findGeneratedHabitsByUserHabit_Habit(Habit habit);
    GeneratedHabit findGeneratedHabitByUserHabit_HabitAndStartIsBetween(Habit habit, OffsetDateTime startDay, OffsetDateTime endDay);
    List<GeneratedHabit> findAllByUserHabit_HabitAndStartAfter(Habit habit, OffsetDateTime today);
    List<GeneratedHabit> findAllByStartIsBetweenAndUserHabit_User_IdAndStatus(OffsetDateTime startDay, OffsetDateTime endDay, Long userId, HabitStatus status);
    List<GeneratedHabit> findAllByStatusAndStartIsBetween(HabitStatus status, OffsetDateTime startDay, OffsetDateTime endDay);
    List<GeneratedHabit> findAllByUserHabitAndStartIsBetweenAndStatusIsNot(UserHabit userHabit, OffsetDateTime startDay, OffsetDateTime endDay, HabitStatus status);
    void deleteAllByUserHabit_Habit_IdAndStartGreaterThanEqual(Long habitId, OffsetDateTime today);
}
