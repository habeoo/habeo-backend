package com.habeo.backend.habits.generated.services;

import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.scheme.models.UserHabit;

import java.util.List;

public interface HabitScheduleService {
    HabitSchedule createHabitSchedule(UserHabit userHabit, String cron);
    HabitSchedule getHabitSchedule(Long scheduleId, Long userId);
    HabitSchedule getHabitScheduleByHabitId(Long habitId, Long userId);
    HabitSchedule getHabitScheduleByHabitId(Long habitId);
    List<HabitSchedule> getAllSchedules(Long userId);
    HabitSchedule updateHabitScheduleCron(Long scheduleId, String cron, Long userId);
}
