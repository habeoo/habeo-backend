package com.habeo.backend.habits.generated.services;

import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.scheme.models.UserHabit;

import java.security.Principal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface HabitEventService {
    List<GeneratedHabit> getGlobalPendingHabits();
    Optional<List<GeneratedHabit>> getAllGeneratedHabits(Long userId);
    Optional<List<GeneratedHabit>> getTodayHabits(Long userId);
    List<GeneratedHabit> getHabitEventsByDate(LocalDate date, Long userId);
    List<GeneratedHabit> getPeriodicHabitEventsByUserHabit(UserHabit userHabit, OffsetDateTime start, OffsetDateTime end);
    Optional<List<GeneratedHabit>> getHabitEvents(Long habitId, Long userId);
    GeneratedHabit getGeneratedHabit(Long id, Long userId);
    GeneratedHabit updateGeneratedHabitStart(Long id, Long userId, OffsetTime start);
    GeneratedHabit updateDailyEntry(Long id, Long userId, String description);
    void deleteGeneratedHabits(Long habitId);
}
