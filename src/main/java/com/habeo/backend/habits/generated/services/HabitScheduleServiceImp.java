package com.habeo.backend.habits.generated.services;

import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.repositories.HabitScheduleRepository;
import com.habeo.backend.habits.scheme.models.UserHabit;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HabitScheduleServiceImp implements HabitScheduleService {
    private final HabitScheduleRepository habitScheduleRepository;

    @Override
    public HabitSchedule createHabitSchedule(UserHabit userHabit, String cron) {
        HabitSchedule habitSchedule = HabitSchedule.builder()
                .cron(cron)
                .userHabit(userHabit)
                .build();

        return habitScheduleRepository.save(habitSchedule);
    }

    @Override
    public HabitSchedule getHabitSchedule(Long scheduleId, Long userId) {
        return habitScheduleRepository.findHabitScheduleByIdAndUserHabit_User_Id(scheduleId, userId)
                .orElseThrow(() -> new NotFoundException("Habit schedule could not be found"));
    }

    @Override
    public HabitSchedule getHabitScheduleByHabitId(Long habitId, Long userId) {
        return habitScheduleRepository.findHabitScheduleByUserHabit_Habit_IdAndUserHabit_User_Id(habitId, userId)
                .orElseThrow(() -> new NotFoundException("Habit schedule could not be found"));
    }

    @Override
    public HabitSchedule getHabitScheduleByHabitId(Long habitId) {
        return habitScheduleRepository.findHabitScheduleByUserHabit_Habit_Id(habitId);
    }

    @Override
    public List<HabitSchedule> getAllSchedules(Long userId) {
        return habitScheduleRepository.findAllByUserHabit_User_Id(userId);
    }

    @Override
    public HabitSchedule updateHabitScheduleCron(Long scheduleId, String cron, Long userId) {
        HabitSchedule habitSchedule = getHabitSchedule(scheduleId, userId);
        habitSchedule.setCron(cron);
        return habitScheduleRepository.save(habitSchedule);
    }
}
