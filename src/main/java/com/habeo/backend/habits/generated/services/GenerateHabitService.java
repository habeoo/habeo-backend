package com.habeo.backend.habits.generated.services;

import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;

import java.time.OffsetTime;
import java.util.List;
import java.util.Optional;

public interface GenerateHabitService {
    Optional<GeneratedHabit> updateGeneratedHabit(Long id, GeneratedHabit editedGeneratedHabit);
    List<GeneratedHabit> updateGeneratedHabitsStart(HabitSchedule habit);
    void generateHabit(HabitSchedule habit);
    GeneratedHabit wrappedHabitEvent(Long id);
}
