package com.habeo.backend.habits.generated.models;

public enum HabitStatus {
    pending,
    done,
    frozen
}
