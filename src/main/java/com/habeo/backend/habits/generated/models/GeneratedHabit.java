package com.habeo.backend.habits.generated.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.habits.scheme.models.UserHabit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "generated_habits")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GeneratedHabit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private OffsetDateTime start;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_habit", referencedColumnName = "id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private UserHabit userHabit;

    private String description;

    @Column(name = "event_status")
    @Enumerated(EnumType.STRING)
    private HabitStatus status;
}
