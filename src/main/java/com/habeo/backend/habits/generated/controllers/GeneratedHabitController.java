package com.habeo.backend.habits.generated.controllers;

import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.services.HabitEventService;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.services.TagService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/habits")
public class GeneratedHabitController {
    private final HabitEventService habitEventsService;

    private final TagService tagService;

    @GetMapping("/{habitId}/events")
    public ResponseEntity<List<GeneratedHabit>> getHabitEvents(@PathVariable Long habitId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Optional<List<GeneratedHabit>> habitEvents = habitEventsService.getHabitEvents(habitId, user.getId());
        return ResponseEntity.ok(habitEvents.get());
    }

    @GetMapping("/events")
    public ResponseEntity<List<GeneratedHabit>> getAllHabitEvents(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Optional<List<GeneratedHabit>> events = habitEventsService.getAllGeneratedHabits(user.getId());
        return ResponseEntity.ok(events.get());
    }

    @GetMapping("/today")
    public ResponseEntity<List<GeneratedHabit>> getTodaysHabits(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Optional<List<GeneratedHabit>> todayHabits = habitEventsService.getTodayHabits(user.getId());
        return ResponseEntity.ok(todayHabits.get());
    }

    @GetMapping("events/{date}")
    public ResponseEntity<List<GeneratedHabit>> getDatesEvents(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        List<GeneratedHabit> events = habitEventsService.getHabitEventsByDate(date, user.getId());
        return ResponseEntity.ok(events);
    }

    @PatchMapping("/events/{eventId}")
    public ResponseEntity<GeneratedHabit> updateDescription(@PathVariable Long eventId, @RequestBody String description, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        GeneratedHabit habitEvent = habitEventsService.updateDailyEntry(eventId, user.getId(), description);
        return ResponseEntity.ok(habitEvent);
    }
}
