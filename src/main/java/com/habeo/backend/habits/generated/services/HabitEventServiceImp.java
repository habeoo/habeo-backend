package com.habeo.backend.habits.generated.services;

import com.habeo.backend.exceptions.BusinessException;
import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.repositories.GeneratedHabitRepository;
import com.habeo.backend.habits.generated.repositories.HabitScheduleRepository;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.services.HabitService;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HabitEventServiceImp implements HabitEventService {
    private final GenerateHabitServiceImpl generateHabitService;

    private final GeneratedHabitRepository generatedHabitRepository;

    private final UserHabitsService userHabitsService;

    @Autowired
    private final HabitService habitService;

    @Override
    public List<GeneratedHabit> getGlobalPendingHabits() {
        OffsetDateTime dayStart = getTodayDateWithTime(0, 0);
        OffsetDateTime dayEnd = getTodayDateWithTime(23, 59);
        return generatedHabitRepository.findAllByStatusAndStartIsBetween(HabitStatus.pending, dayStart, dayEnd);
    }

    @Override
    public Optional<List<GeneratedHabit>> getAllGeneratedHabits(Long userId) {
        List<Habit> userHabits = userHabitsService.getAllHabits(userId);

        List<GeneratedHabit> habitEvents = new ArrayList<>();
        userHabits.forEach(h -> habitEvents.addAll(generatedHabitRepository.findGeneratedHabitsByUserHabit_Habit(h)));

        return Optional.of(habitEvents);
    }

    @Override
    public Optional<List<GeneratedHabit>> getTodayHabits(Long userId) {
        List<GeneratedHabit> habits = new ArrayList<>();
        OffsetDateTime today = OffsetDateTime.now();

        getAllGeneratedHabits(userId).get()
                .forEach(
                        generatedHabit -> {
                            if (generatedHabit.getStatus().equals(HabitStatus.pending)) {
                                if (todayMatchesEventDate(today, generatedHabit.getStart())) {
                                    habits.add(generatedHabit);
                                }
                            }
                        });

        return Optional.of(habits);
    }

    @Override
    public List<GeneratedHabit> getHabitEventsByDate(LocalDate date, Long userId) {
        OffsetDateTime dayStart = OffsetDateTime.of(date, LocalTime.MIDNIGHT, ZoneOffset.UTC);
        OffsetDateTime dayEnd = OffsetDateTime.of(date, LocalTime.of(23, 59), ZoneOffset.UTC);
        return generatedHabitRepository.findAllByStartIsBetweenAndUserHabit_User_IdAndStatus(dayStart, dayEnd, userId, HabitStatus.pending);
    }

    @Override
    public List<GeneratedHabit> getPeriodicHabitEventsByUserHabit(UserHabit userHabit, OffsetDateTime start, OffsetDateTime end) {
        return generatedHabitRepository.findAllByUserHabitAndStartIsBetweenAndStatusIsNot(userHabit, start, end, HabitStatus.pending);
    }

    @Override
    @Transactional
    public Optional<List<GeneratedHabit>> getHabitEvents(Long habitId, Long userId) {
        Habit habit = habitService.getWrappedHabit(habitId);
        if (userHabitsService.habitBelongsToUser(userId, habit)) {
            return Optional.of(generatedHabitRepository.findGeneratedHabitsByUserHabit_Habit(habit)
                    .stream()
                    .toList());
        }
        throw new ForbiddenResourceException("habit does not belong to you");
    }

    @Override
    public GeneratedHabit getGeneratedHabit(Long id, Long userId) {
        GeneratedHabit generatedHabit = generateHabitService.wrappedHabitEvent(id);
        if (userHabitsService.habitBelongsToUser(userId, generatedHabit.getUserHabit().getHabit())) {
            return generatedHabitRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException("Habit event could not be found"));
        }
        throw new ForbiddenResourceException("habit event does not belong to you");
    }

    @Override
    public GeneratedHabit updateGeneratedHabitStart(Long id, Long userId, OffsetTime start) {
        GeneratedHabit generatedHabit = getGeneratedHabit(id, userId);
        generatedHabit.setStart(generatedHabit.getStart()
                .withOffsetSameInstant(start.getOffset())
                .withHour(start.getHour())
                .withMinute(start.getMinute()));
        return generatedHabitRepository.save(generatedHabit);
    }

    @Override
    public GeneratedHabit updateDailyEntry(Long id, Long userId, String description) {
        GeneratedHabit generatedHabit = getGeneratedHabit(id, userId);
        generatedHabit.setDescription(description);
        return generatedHabitRepository.save(generatedHabit);
    }

    @Override
    public void deleteGeneratedHabits(Long habitId) {
        OffsetDateTime today = OffsetDateTime.now().withHour(0).withMinute(0);
        generatedHabitRepository.deleteAllByUserHabit_Habit_IdAndStartGreaterThanEqual(habitId, today);
    }

    private boolean todayMatchesEventDate(OffsetDateTime today, OffsetDateTime start) {
        return start.getYear() == today.getYear()
                && start.getMonth() == today.getMonth()
                && start.getDayOfMonth() == today.getDayOfMonth();
    }

    private OffsetDateTime getTodayDateWithTime(int hour, int minute) {
        return OffsetDateTime.now().withHour(hour).withMinute(minute).withSecond(0).withNano(0);
    }
}

