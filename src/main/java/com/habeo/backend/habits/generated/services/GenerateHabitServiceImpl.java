package com.habeo.backend.habits.generated.services;

import com.google.api.client.util.Lists;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.repositories.GeneratedHabitRepository;
import com.habeo.backend.habits.generated.repositories.HabitScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.*;
import java.util.*;

@Service
@RequiredArgsConstructor
public class GenerateHabitServiceImpl implements GenerateHabitService {
    private final GeneratedHabitRepository generatedHabitRepository;

    private final HabitScheduleRepository habitScheduleRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional
    public Optional<GeneratedHabit> updateGeneratedHabit(Long id, GeneratedHabit editedGeneratedHabit) {
        GeneratedHabit habit = generatedHabitRepository.findById(id).orElseThrow(() -> new NotFoundException("Habit event could not be found"));
        habit.setStart(editedGeneratedHabit.getStart());
        generatedHabitRepository.save(habit);
        return Optional.of(habit);
    }

    @Override
    public List<GeneratedHabit> updateGeneratedHabitsStart(HabitSchedule habit) {
        Iterator<OffsetDateTime> startTime = convertCronToDateTime(habit.getCron()).iterator();
        OffsetDateTime today = OffsetDateTime.now().minusDays(1);
        Iterator<GeneratedHabit> generatedHabits = generatedHabitRepository.findAllByUserHabit_HabitAndStartAfter(habit.getUserHabit().getHabit(), today).iterator();
        while (generatedHabits.hasNext() && startTime.hasNext()) {
            GeneratedHabit event = generatedHabits.next();
            event.setStart(startTime.next());
            generatedHabitRepository.save(event);
        }

        return Lists.newArrayList(generatedHabits);
    }

    @Scheduled(cron = "0 0 0 1 * ?")
    public void generateAllHabits() {
        List<HabitSchedule> schedules = habitScheduleRepository.findAllByUserHabit_Habit_Active(true);
        schedules.forEach(this::generateHabit);
    }

    @Override
    public void generateHabit(HabitSchedule habitSchedule) {
        List<OffsetDateTime> startTime = convertCronToDateTime(habitSchedule.getCron());
        GeneratedHabit generatedHabit;
        for (OffsetDateTime start : startTime) {
            generatedHabit = new GeneratedHabit();
            generatedHabit.setStart(start);
            generatedHabit.setUserHabit(habitSchedule.getUserHabit());
            generatedHabit.setStatus(HabitStatus.pending);
            generatedHabitRepository.save(generatedHabit);
        }
    }

    @Override
    public GeneratedHabit wrappedHabitEvent(Long id) {
        return entityManager.getReference(GeneratedHabit.class, id);
    }

    private List<OffsetDateTime> convertCronToDateTime(String cron) {
        CronExpression cronExpression = CronExpression.parse(cron);
        OffsetDateTime dateTime = OffsetDateTime.of(LocalDate.now().minusDays(1), LocalTime.MAX, ZoneOffset.UTC);
        OffsetDateTime firstDate = cronExpression.next(dateTime);
        List<OffsetDateTime> dates = new ArrayList<>(Collections.singleton(firstDate));
        OffsetDateTime current = firstDate;
        while (current.minusMonths(1).isBefore(firstDate)) {
            current = cronExpression.next(current);
            dates.add(current);
        }
        return dates;
    }
}
