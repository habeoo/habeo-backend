package com.habeo.backend.habits.scheme.models.dtos;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;

import javax.validation.constraints.Pattern;

@JsonTypeName("measurable")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MeasurableHabitDto extends HabitDto{
    @Pattern(regexp = "^[a-z/\s]+$",
            message = "Only lowercase letters and / allowed")
    private String unit;
    private int target;
}
