package com.habeo.backend.habits.scheme.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user_habits")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserHabit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Habit habit;

    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Tag tag;
}
