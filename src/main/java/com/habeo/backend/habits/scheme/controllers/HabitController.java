package com.habeo.backend.habits.scheme.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.services.GenerateHabitService;
import com.habeo.backend.habits.generated.services.HabitEventService;
import com.habeo.backend.habits.generated.services.HabitScheduleService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.habits.scheme.services.HabitService;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import com.habeo.backend.habits.scheme.validation.HabitDtoValidator;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.services.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/habits")
public class HabitController {
    private final HabitService habitService;

    private HabitDtoValidator habitValidator;

    private final TagService tagService;

    private final UserHabitsService userHabitsService;

    private final HabitScheduleService habitScheduleService;

    private final GenerateHabitService generatedHabitService;

    private final HabitEventService habitEventService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(habitValidator);
    }

    @PostMapping
    public ResponseEntity<Habit> createNewHabit(@Valid @RequestBody HabitDto habit, Principal principal) throws IOException {
        User user = tagService.getUserFromPrincipal(principal);
        Habit created = habitService.createHabit(habit);
        Tag tag = tagService.getWrappedTag(habit.getTagId());

        UserHabit userHabit = userHabitsService.saveUserHabit(user, created, tag);
        HabitSchedule habitSchedule = habitScheduleService.createHabitSchedule(userHabit, habit.getCron());
        generatedHabitService.generateHabit(habitSchedule);
        return ResponseEntity.created(URI.create("/habits/" + created.getId())).body(created);
    }

//    @PutMapping("/move")
//    public ResponseEntity<Habit> moveHabitToTag(@RequestBody @Valid HabitDto habitDto) {
//        Optional<Habit> moved = habitService.moveHabitToTag(habitDto);
//
//        return ResponseEntity.created(URI.create("/habits/" + moved.get().getId())).body(moved.get());
//    }

    @GetMapping
    public ResponseEntity<List<UserHabit>> getAllHabits(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        List<UserHabit> habits = userHabitsService.getAllUserHabits(user.getId());
        return ResponseEntity.ok().body(habits);
    }

    @GetMapping("/{habitId}")
    public ResponseEntity<HabitSchedule> getHabitById(@PathVariable Long habitId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
//        Optional<Habit> habit = habitService.getHabitById(habitId, user.getId());
        return ResponseEntity.ok().body(habitScheduleService.getHabitScheduleByHabitId(habitId, user.getId()));
    }

    @PutMapping("/{habitId}")
    public ResponseEntity<Habit> editHabit(@PathVariable Long habitId, @Validated @RequestBody HabitDto editedHabit, Principal principal) throws JsonProcessingException {
        User user = tagService.getUserFromPrincipal(principal);

        Optional<Habit> habit = habitService.updateHabit(habitId, editedHabit, user.getId());
        return ResponseEntity.ok().body(habit.get());
    }

    @PatchMapping("/{habitId}/cancel")
    @Transactional
    public ResponseEntity<Void> cancelHabit(@PathVariable Long habitId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        habitService.cancelHabit(habitId, user.getId());
        habitEventService.deleteGeneratedHabits(habitId);
        return ResponseEntity.ok().build();
    }
}
