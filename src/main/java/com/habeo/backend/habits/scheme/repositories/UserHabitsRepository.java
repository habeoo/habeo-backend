package com.habeo.backend.habits.scheme.repositories;

import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserHabitsRepository extends JpaRepository<UserHabit, Long> {
    UserHabit findAllByHabit_IdAndUser_Id(Long habit, Long user);

    List<UserHabit> findAllByUser(User user);
}
