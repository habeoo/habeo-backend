package com.habeo.backend.habits.scheme.models.dtos;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property="type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = MeasurableHabitDto.class, name = "measurable"),
        @JsonSubTypes.Type(value = BooleanHabitDto.class, name = "boolean"),
})
@Valid
public abstract class HabitDto {
    @Pattern(regexp = "^[a-zA-Z,.\s]+$",
            message = "Only letters, comma and dot allowed")
    private String name;
    @Pattern(regexp = "^[a-zA-Z,.\s]+$",
            message = "Only letters, comma and dot allowed")
    private String goal;
    private Long tagId;
    private String cron;
    private String type;
}
