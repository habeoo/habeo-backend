package com.habeo.backend.habits.scheme.validation;

import com.habeo.backend.exceptions.BusinessException;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class HabitDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return HabitDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
//        HabitDto habit = (HabitDto) target;
//        if (habit.getStartTime().isAfter(habit.getEndTime())) {
//            throw new BusinessException(
//                    HttpStatus.BAD_REQUEST,
//                    "habit cannot be created because start time is after end time"
//            );
//        }
    }
}
