package com.habeo.backend.habits.scheme.services;

import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;

import java.util.List;

public interface UserHabitsService {
    public UserHabit saveUserHabit(User user, Habit habit, Tag tag);

    public UserHabit getUserHabit(Long habitId, Long userId);

    boolean habitBelongsToUser(Long userId, Habit habit);

    List<UserHabit> getAllUserHabits(Long userId);

    List<Habit> getAllHabits(Long userId);
}
