package com.habeo.backend.habits.scheme.repositories;

import com.habeo.backend.habits.scheme.models.Habit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HabitRepository extends JpaRepository<Habit, Long> {
}
