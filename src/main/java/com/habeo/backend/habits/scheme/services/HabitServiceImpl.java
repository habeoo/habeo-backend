package com.habeo.backend.habits.scheme.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.services.GenerateHabitService;
import com.habeo.backend.habits.generated.services.HabitScheduleService;
import com.habeo.backend.habits.scheme.models.BooleanHabit;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.MeasurableHabit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.models.dtos.BooleanHabitDto;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.habits.scheme.models.dtos.MeasurableHabitDto;
import com.habeo.backend.habits.scheme.repositories.HabitRepository;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.services.TagServiceImpl;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HabitServiceImpl implements HabitService {
    private final HabitRepository habitRepository;

    @Autowired
    private EntityManager entityManager;

    private final TagServiceImpl tagService;

    private final GenerateHabitService generatedHabitService;

    private final UserHabitsService userHabitsService;

    private final HabitScheduleService habitScheduleService;

    @Autowired
    private final ObjectMapper mapper;

    @Override
    @Transactional
    public Habit createHabit(HabitDto habit) throws IOException {
        Habit createdHabit = null;
        String habitJson = mapper.writeValueAsString(habit);

        if (habit instanceof BooleanHabitDto) {
            BooleanHabit booleanHabit = mapper.readValue(habitJson, BooleanHabit.class);
            createdHabit = habitRepository.save((Habit) booleanHabit);
        } else if (habit instanceof MeasurableHabitDto) {
            MeasurableHabit measurableHabit = mapper.readValue(habitJson, MeasurableHabit.class);
            createdHabit = habitRepository.save((Habit) measurableHabit);
        }

        return createdHabit;
    }

    @Override
    public Optional<Habit> getHabitById(Long id, Long userId) {
        Optional<Habit> habit = habitRepository.findById(id);

        if (habit.isPresent() && userHabitsService.habitBelongsToUser(userId, habit.get()))
            return habit;

        throw new ForbiddenResourceException("habit does not belong to you");
    }

    @Override
    @Transactional
    public Optional<Habit> updateHabit(Long habitId, HabitDto habitDto, Long userId) throws JsonProcessingException {
        Habit habit = getWrappedHabit(habitId);
        String habitJson = mapper.writeValueAsString(habitDto);

        HabitSchedule habitSchedule = habitScheduleService.getHabitScheduleByHabitId(habitId,  userId);
        habitScheduleService.updateHabitScheduleCron(habitSchedule.getId(), habitDto.getCron(), userId);
        generatedHabitService.updateGeneratedHabitsStart(habitSchedule);

        if (userHabitsService.habitBelongsToUser(userId, habit)) {
            habit.setName(habitDto.getName());
            habit.setGoal(habitDto.getGoal());

            if (habitDto instanceof MeasurableHabitDto) {
                MeasurableHabit measurableHabit = (MeasurableHabit) habitRepository.findById(habitId).get();
                MeasurableHabit modifiedMeasurableHabit = mapper.readValue(habitJson, MeasurableHabit.class);
                measurableHabit.setUnit(modifiedMeasurableHabit.getUnit());
                measurableHabit.setTarget(modifiedMeasurableHabit.getTarget());
                habitRepository.save(measurableHabit);
            }
        }

        return Optional.of(habitRepository.save(habit));
    }

//    @Override
//    public Optional<Habit> copyHabitToTag(HabitDto habitDto) {
//        return Optional.empty();
//    }
//
//    @Override
//    public Optional<Habit> moveHabitToTag(HabitDto habitDto) {
//        return Optional.empty();
//    }

//    @Override
//    @Transactional
//    public Optional<Habit> moveHabitToTag(HabitDto habitDto) {
//        Optional<Habit> movedHabit = copyHabitToTag(habitDto);
//        habitRepository.deleteByTag(movedHabit.get().getTag());
//        if (movedHabit.isPresent()) {
//            return movedHabit;
//        }
//
//        throw new BusinessException(HttpStatus.BAD_REQUEST, "Couldn't move habit to tag");
//    }

    @Override
    public Habit getWrappedHabit(Long habitId) {
        return entityManager.unwrap(Session.class).getReference(Habit.class, habitId);
    }

    @Override
    public void cancelHabit(Long habitId, Long userId) {
        Habit habit = getHabitById(habitId, userId).get();
        habit.setActive(false);
        habitRepository.save(habit);
    }
}
