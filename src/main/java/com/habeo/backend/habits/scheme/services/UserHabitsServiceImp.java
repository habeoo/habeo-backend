package com.habeo.backend.habits.scheme.services;

import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.repositories.UserHabitsRepository;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.services.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserHabitsServiceImp implements UserHabitsService {
    private final UserHabitsRepository userHabitsRepository;

    private final TagService tagService;

    @Autowired
    private EntityManager entityManager;

    @Override
    public UserHabit saveUserHabit(User user, Habit habit, Tag tag) {
        UserHabit userHabit = UserHabit.builder()
                .habit(habit)
                .user(user)
                .tag(tag)
                .build();
        return userHabitsRepository.save(userHabit);
    }

    @Override
    public UserHabit getUserHabit(Long habitId, Long userId) {
        return userHabitsRepository.findAllByHabit_IdAndUser_Id(habitId, userId);
    }

    @Override
    public boolean habitBelongsToUser(Long userId, Habit habit) {
        UserHabit u = userHabitsRepository.findAllByHabit_IdAndUser_Id(habit.getId(), userId);
        if (u == null) {
            throw new ForbiddenResourceException("You cannot access habits that are not yours");
        }
        return true;
    }

    @Override
    @Transactional
    public List<UserHabit> getAllUserHabits(Long userId) {
        User user = tagService.getWrappedUser(userId);
        return userHabitsRepository.findAllByUser(user);
    }

    @Override
    @Transactional
    public List<Habit> getAllHabits(Long userId) {
        User user = tagService.getWrappedUser(userId);
        return userHabitsRepository.findAllByUser(user)
                .stream()
                .map(UserHabit::getHabit).toList();
    }

}
