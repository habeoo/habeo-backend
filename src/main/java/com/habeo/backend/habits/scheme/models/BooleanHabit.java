package com.habeo.backend.habits.scheme.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue("boolean")
@Entity
public class BooleanHabit extends Habit{
}
