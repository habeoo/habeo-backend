package com.habeo.backend.habits.scheme.models.dtos;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@JsonTypeName("boolean")
@Getter
@AllArgsConstructor
@Data
@Builder
public class BooleanHabitDto extends HabitDto {
}
