package com.habeo.backend.habits.scheme.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.dtos.HabitDto;
import com.habeo.backend.oauth2.models.User;

import java.io.IOException;
import java.util.Optional;

public interface HabitService {
    Habit createHabit(HabitDto habit) throws IOException;

    Optional<Habit> getHabitById(Long id, Long userId);

    Optional<Habit> updateHabit(Long habitId, HabitDto habit, Long userId) throws JsonProcessingException;

//    Optional<Habit> copyHabitToTag(HabitDto habitDto);
//
//    Optional<Habit> moveHabitToTag(HabitDto habitDto);
    Habit getWrappedHabit(Long id);

    void cancelHabit(Long habitId, Long userId);
}
