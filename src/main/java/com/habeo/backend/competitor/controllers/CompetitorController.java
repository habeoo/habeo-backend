package com.habeo.backend.competitor.controllers;

import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.competitor.services.CompetitorService;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.services.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.security.Principal;

@RestController
@AllArgsConstructor
public class CompetitorController {
    private final TagService tagService;

    private final CompetitorService competitorService;

    @PostMapping("habits/{habitId}/done")
    public ResponseEntity<GeneratedHabit> markHabitEventAsDone(@PathVariable Long habitId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        competitorService.markHabitEventAsDone(habitId, user);
        return ResponseEntity.ok().build();
    }

    @PostMapping("habits/{habitId}/freeze")
    public ResponseEntity<GeneratedHabit> freezeHabit(@PathVariable Long habitId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        competitorService.freezeHabitEvent(habitId, user);
        return ResponseEntity.ok().build();
    }

    @PostMapping("freeze/new")
    public ResponseEntity<Void> buyFreeze(Principal principal) {
        competitorService.buyFreeze(principal);
        return ResponseEntity.created(URI.create("freeze/new")).build();
    }

    @GetMapping("competitor")
    public ResponseEntity<Competitor> getCompetitor(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        return ResponseEntity.ok(competitorService.getCompetitor(user));
    }
}
