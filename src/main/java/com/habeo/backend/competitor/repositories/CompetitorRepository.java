package com.habeo.backend.competitor.repositories;

import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.oauth2.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitorRepository extends JpaRepository<Competitor, Long> {
    Competitor findCompetitorByUser(User user);
}
