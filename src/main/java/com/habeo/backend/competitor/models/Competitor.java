package com.habeo.backend.competitor.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.habeo.backend.oauth2.models.User;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "competitors")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Competitor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;

    @Column(columnDefinition = "integer default 100")
    private Integer points;

    @Column(columnDefinition = "integer default 5")
    private Integer freezes;
}
