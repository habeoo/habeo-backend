package com.habeo.backend.competitor.services;

import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.oauth2.models.User;

import java.security.Principal;
import java.util.Optional;

public interface CompetitorService {
    Competitor createCompetitor(User user);
    GeneratedHabit markHabitEventAsDone(Long habitId, User user);
    GeneratedHabit freezeHabitEvent(Long habitId, User user);
    void buyFreeze(Principal principal);
    Competitor getCompetitor(User user);

    void removePointsForSkippedHabit();
}
