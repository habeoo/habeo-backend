package com.habeo.backend.competitor.services;

import com.habeo.backend.competitor.models.Competitor;
import com.habeo.backend.competitor.repositories.CompetitorRepository;
import com.habeo.backend.exceptions.BusinessException;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.repositories.GeneratedHabitRepository;
import com.habeo.backend.habits.generated.services.HabitEventService;
import com.habeo.backend.habits.generated.services.HabitEventServiceImp;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.services.HabitService;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.services.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.security.Principal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CompetitorServiceImp implements CompetitorService {
    private final HabitService habitService;

    private final CompetitorRepository repository;

    private final EntityManager entityManager;

    private final GeneratedHabitRepository generatedHabitRepository;

    private final TagService tagService;

    private final UserHabitsService userHabitsService;

    private final HabitEventService habitEventService;

    @Override
    @Transactional
    public Competitor createCompetitor(User user) {
        Competitor competitor = Competitor.builder()
                .freezes(5)
                .points(100)
                .user(user)
                .build();
        return repository.save(competitor);
    }

    @Override
    @Transactional
    public GeneratedHabit markHabitEventAsDone(Long habitId, User user) {
        Habit habit = habitService.getWrappedHabit(habitId);
        OffsetDateTime todayStart = getTodayDateWithTime(0, 0);
        OffsetDateTime todayEnd = getTodayDateWithTime(23, 59);
        GeneratedHabit habitEvent = generatedHabitRepository.findGeneratedHabitByUserHabit_HabitAndStartIsBetween(habit, todayStart, todayEnd);

        if (userHabitsService.habitBelongsToUser(user.getId(), habit)) {
            habitEvent.setStatus(HabitStatus.done);
        }

        Competitor competitor = repository.findCompetitorByUser(user);
        updateCompetitor(competitor, 10, 0);
        return generatedHabitRepository.save(habitEvent);
    }

    @Override
    @Transactional
    public GeneratedHabit freezeHabitEvent(Long habitId, User user) {
        Habit habit = entityManager.getReference(Habit.class, habitId);
        if (!userHabitsService.habitBelongsToUser(user.getId(), habit)) {
            throw new BusinessException(HttpStatus.FORBIDDEN, "Habit doesn't belong to you");
        }

        Competitor competitor = repository.findCompetitorByUser(user);
        OffsetDateTime todayStart = getTodayDateWithTime(0, 0);
        OffsetDateTime todayEnd = getTodayDateWithTime(23, 59);
        GeneratedHabit habitEvent = generatedHabitRepository.findGeneratedHabitByUserHabit_HabitAndStartIsBetween(habit, todayStart, todayEnd);

        if (competitor.getFreezes() > 0) {
            updateCompetitor(competitor, 0, -1);
            habitEvent.setStatus(HabitStatus.frozen);
        }

        return generatedHabitRepository.save(habitEvent);
    }

    @Override
    @Transactional
    public void buyFreeze(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Competitor competitor = repository.findCompetitorByUser(user);
        if (competitor.getPoints() > 50) {
            updateCompetitor(competitor, -50, 1);
        } else {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "Not enough points");
        }
        repository.save(competitor);
    }

    @Override
    public Competitor getCompetitor(User user) {
        return repository.findCompetitorByUser(user);
    }

    @Override
    @Scheduled(cron = "0 59 23 * * *")
    public void removePointsForSkippedHabit() {
        List<GeneratedHabit> pendingHabits = habitEventService.getGlobalPendingHabits();
        Map<User, Long> users = getUsersToBePunished(pendingHabits);
        users.forEach((user, skippedHabits) -> {
            Competitor competitor = getCompetitor(user);
            updateCompetitor(competitor, (int) (skippedHabits * 20 * -1), 0);
        });
    }

    public void updateCompetitor(Competitor competitor, int points, int freezes) {
        competitor.setPoints(competitor.getPoints() + points);
        competitor.setFreezes(competitor.getFreezes() + freezes);
        repository.save(competitor);
    }

    private Map<User, Long> getUsersToBePunished(List<GeneratedHabit> generatedHabits) {
        return generatedHabits.stream()
                .map(g -> g.getUserHabit().getUser())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private OffsetDateTime getTodayDateWithTime(int hour, int minute) {
        return OffsetDateTime.now().withHour(hour).withMinute(minute).withSecond(0).withNano(0);
    }
}
