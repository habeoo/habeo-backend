package com.habeo.backend.tags.models.dtos;

import javax.validation.constraints.Pattern;

public record CreateTagDto(
        @Pattern(regexp = "^[a-zA-Z,.\s]+$",
                message = "Only letters, comma and dot allowed")
        String name,
        Boolean defaultTag) {
}
