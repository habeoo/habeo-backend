package com.habeo.backend.tags.controllers;

import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.models.dtos.CreateTagDto;
import com.habeo.backend.tags.services.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/tags")
public class TagController {
    private final TagService tagService;

    @PostMapping
    public ResponseEntity<Tag> createNewTag(@Validated @RequestBody CreateTagDto tag, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Tag createdTag = tagService.createNewTag(tag, user.getId());
        return ResponseEntity
                .created(URI
                        .create("/tags/" + createdTag.getId()))
                .body(createdTag);
    }

    @GetMapping
    public ResponseEntity<List<Tag>> getAllTags(Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Optional<List<Tag>> tags = tagService.getAllTags(user.getId());
        return ResponseEntity.ok().body(tags.get());
    }

    @GetMapping("/{tagId}")
    public ResponseEntity<Tag> getTagById(@PathVariable Long tagId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Tag tag = tagService.getTagById(tagId, user.getId());
        return ResponseEntity.ok().body(tag);
    }

    @PutMapping("/{tagId}")
    public ResponseEntity<Tag> editTag(@PathVariable Long tagId, @RequestBody CreateTagDto tag, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        Tag editedTag = tagService.updateTag(tagId, tag, user.getId());
        return ResponseEntity.ok().body(editedTag);
    }

    @DeleteMapping("/{tagId}")
    public void deleteTag(@PathVariable Long tagId, Principal principal) {
        User user = tagService.getUserFromPrincipal(principal);
        tagService.deleteTag(tagId, user.getId());
    }
}
