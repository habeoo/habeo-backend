package com.habeo.backend.tags.repositories;

import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findAllByUser(User user);

    Optional<Tag> findTagByIdAndUser(Long id, User user);

    void deleteTagByIdAndUser(Long id, User user);

    Tag findTagByUser_IdAndAndDefaultTag(Long userId, boolean defaultTag);

}
