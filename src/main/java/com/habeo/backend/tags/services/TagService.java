package com.habeo.backend.tags.services;

import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.dtos.CreateTagDto;
import com.habeo.backend.tags.models.Tag;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface TagService {
    Tag createNewTag(CreateTagDto tag, Long userId);
    Optional<Tag> createDefaultTag(Long userId);
    Optional<List<Tag>> getAllTags(Long userId);
    Tag getTagById(Long id, Long userId);
    Tag updateTag(Long tagId, CreateTagDto tag, Long userId);
    void deleteTag(Long id, Long userId);
    Tag getWrappedTag(Long tagId);
    User getUserFromPrincipal(Principal principal);
    User getWrappedUser(Long userId);
    Tag getUsersDefaultTag(Long userId);
}
