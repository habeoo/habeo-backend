package com.habeo.backend.tags.services;

import com.habeo.backend.exceptions.BusinessException;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.oauth2.security.UserPrincipal;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.models.dtos.CreateTagDto;
import com.habeo.backend.tags.repositories.TagRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Tag createNewTag(CreateTagDto createTagDto, Long userId) {
        User user = getWrappedUser(userId);

        Tag tag =
                Tag.builder()
                        .name(createTagDto.name())
                        .user(user)
                        .defaultTag(createTagDto.defaultTag())
                        .build();
        entityManager.unwrap(Session.class).save(tag);
        return tag;
    }

    @Override
    public Optional<Tag> createDefaultTag(Long userId) {
        CreateTagDto tag =
                new CreateTagDto(
                        "Uncategorized",
                        true);

        return Optional.ofNullable(createNewTag(tag, userId));
    }

    @Override
    public Optional<List<Tag>> getAllTags(Long userId) {
        User user = getWrappedUser(userId);
        return Optional.of(tagRepository.findAllByUser(user));
    }

    @Override
    @Transactional
    public Tag getTagById(Long id, Long userId) {
        User user = getWrappedUser(userId);
        Tag tag = tagRepository.findTagByIdAndUser(id, user)
                .orElseThrow(() -> new NotFoundException("It seems you don't have such tag"));
        return tag;
    }

    @Override
    @Transactional
    public Tag updateTag(Long tagId, CreateTagDto tag, Long userId) {
        User user = getWrappedUser(userId);
        Tag oldTag = tagRepository.findTagByIdAndUser(tagId, user)
                .orElseThrow(() -> new NotFoundException("It seems you don't have such tag"));
        oldTag.setName(tag.name());
        return tagRepository.save(oldTag);
    }

    @Override
    @Transactional
    public void deleteTag(Long id, Long userId) {
        if (tagBelongsToUser(id, userId))
            tagRepository.deleteById(id);
    }

    @Override
    public Tag getWrappedTag(Long tagId) {
        return entityManager.getReference(Tag.class, tagId);
    }

    @Override
    public User getUserFromPrincipal(Principal principal) {
        return entityManager.getReference(
                User.class,
                ((UserPrincipal) ((UsernamePasswordAuthenticationToken) principal).getPrincipal())
                        .getId());
    }

    @Override
    public User getWrappedUser(Long userId) {
        return entityManager.getReference(User.class, userId);
    }

    @Override
    public Tag getUsersDefaultTag(Long userId) {
        return tagRepository.findTagByUser_IdAndAndDefaultTag(userId, true);
    }

    private boolean tagBelongsToUser(Long tagId, Long userId) {
        Tag tag = getWrappedTag(tagId);
        return tag.getUser().getId() == userId;
    }
}
