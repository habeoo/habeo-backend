package com.habeo.backend;

import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.oauth2.repositories.UserRepository;
import com.habeo.backend.oauth2.security.UserPrincipal;
import com.habeo.backend.oauth2.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@AllArgsConstructor
public class YayController {
  private final UserRepository userRepository;

  @GetMapping("/")
  public String index() {
    return "Yayyyy! It works!";
  }

  @RequestMapping(value = "/user")
  public User user(Principal principal) {
    return userRepository
        .findById(
            ((UserPrincipal) ((UsernamePasswordAuthenticationToken) principal).getPrincipal())
                .getId())
        .get();
  }
}
