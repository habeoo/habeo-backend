package com.habeo.backend.oauth2.payload;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoogleLoginRequest implements Serializable {
    @NotBlank
    private String accessToken;
}
