package com.habeo.backend.oauth2.payload;

import lombok.*;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
}
