package com.habeo.backend.oauth2;

import com.habeo.backend.oauth2.payload.GoogleLoginRequest;
import com.habeo.backend.oauth2.payload.JwtAuthenticationResponse;
import com.habeo.backend.oauth2.services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;

@RestController
@AllArgsConstructor
@Slf4j
public class AuthController {
    private final UserService userService;

    private final JwtAuthenticationResponse authenticationResponse;

    @PostMapping("/google/signin/{accessToken}")
    public ResponseEntity<?> facebookAuth(@Valid @PathVariable String accessToken) throws IOException {
        OAuth2User user = userService.loadUser(accessToken);
        if (user != null)
            return ResponseEntity.ok(authenticationResponse.getAccessToken());
        return ResponseEntity.badRequest().build();
    }
}
