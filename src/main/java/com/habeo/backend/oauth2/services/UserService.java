package com.habeo.backend.oauth2.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;
import com.habeo.backend.competitor.services.CompetitorService;
import com.habeo.backend.exceptions.ResourceNotFoundException;
import com.habeo.backend.oauth2.models.AuthProvider;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.oauth2.payload.JwtAuthenticationResponse;
import com.habeo.backend.oauth2.repositories.UserRepository;
import com.habeo.backend.oauth2.security.UserPrincipal;
import com.habeo.backend.oauth2.security.jwt.JwtTokenProvider;
import com.habeo.backend.tags.services.TagService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    private final JwtAuthenticationResponse authenticationResponse;

    private final JwtTokenProvider tokenProvider;

    private final TagService tagService;

    private final CompetitorService competitorService;

    public OAuth2User loadUser(String accessToken) throws OAuth2AuthenticationException, IOException {
        GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
        Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();
        Userinfo userInfo = oauth2.userinfo().get().execute();
        UserDetails userDetails = (UserDetails) processOAuth2User(userInfo);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails, userDetails.getAuthorities());
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);
        authenticationResponse.setAccessToken(tokenProvider.generateToken(securityContext.getAuthentication()));
        try {
            return processOAuth2User(userInfo);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(Userinfo userInfo) {
        Optional<User> userOptional = userRepository.findByEmail(userInfo.getEmail());
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            user = updateExistingUser(user, userInfo);
        } else {
            user = registerNewUser(userInfo);
            tagService.createDefaultTag(user.getId());
            competitorService.createCompetitor(user);
        }
        return UserPrincipal.create(user);
    }

    private User registerNewUser(Userinfo userInfo) {
        User user = new User();

        user.setProvider(AuthProvider.valueOf("google"));
        user.setName(userInfo.getName());
        user.setEmail(userInfo.getEmail());
        user.setPhotoUrl(String.valueOf(userInfo.getPicture()));
        return userRepository.save(user);
    }

    private User updateExistingUser(User existingUser, Userinfo user) {
        existingUser.setName(user.getName());
        existingUser.setPhotoUrl(String.valueOf(user.getPicture()));
        return userRepository.save(existingUser);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", id)
        );

        return UserPrincipal.create(user);
    }
}
