package com.habeo.backend.oauth2.models;

public enum AuthProvider {
    facebook,
    google,
}
