package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.services.HabitEventServiceImp;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
public class HabitEventServiceTest {
    @Autowired
    private HabitEventServiceImp habitEventService;

    @Autowired
    DBService dbService;


    @Test
    @Transactional
    void getAllGeneratedHabits__Should_ReturnHabitEvents_When_Saved() {
        User user = dbService.insertUser();

        Optional<List<GeneratedHabit>> result = habitEventService.getAllGeneratedHabits(user.getId());

        assertTrue(result.isPresent());
    }

    @Test
    @Transactional
    void getTodayHabits__Should_ReturnHabitEventsToBeDoneToday_When_Present() {
        User user = dbService.insertUser();
        Long userId = user.getId();

        Optional<List<GeneratedHabit>> result = habitEventService.getTodayHabits(userId);

        assertTrue(result.isPresent());
    }

    @Test
    @Transactional
    void getHabitEvents__Should_ReturnHabitEvents_When_HabitHasEvents() {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit h = dbService.insertHabit();

        dbService.insertUserHabit(h, user, tag);

        Optional<List<GeneratedHabit>> result = habitEventService.getHabitEvents(h.getId(), user.getId());

        assertTrue(result.isPresent());
    }
}
