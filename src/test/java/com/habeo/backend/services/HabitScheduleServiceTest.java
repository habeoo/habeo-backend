package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.repositories.HabitScheduleRepository;
import com.habeo.backend.habits.generated.services.HabitScheduleService;
import com.habeo.backend.habits.generated.services.HabitScheduleServiceImp;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class HabitScheduleServiceTest {
    @Mock
    private HabitScheduleRepository habitScheduleRepository;

    @Autowired
    private DBService dbService;

    @Test
    @Transactional
    void testCreateHabitSchedule() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit userHabit = dbService.insertUserHabit(habit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);

        when(habitScheduleRepository.save(any(HabitSchedule.class))).thenReturn(habitSchedule);

        HabitScheduleService habitScheduleService = new HabitScheduleServiceImp(habitScheduleRepository);

        HabitSchedule result = habitScheduleService.createHabitSchedule(userHabit, "0 0 8 * * 1");

        assertEquals(habitSchedule.getCron(), result.getCron());
        assertEquals(habitSchedule.getUserHabit(), result.getUserHabit());
        verify(habitScheduleRepository, times(1)).save(any(HabitSchedule.class));
    }

    @Test
    @Transactional
    void testGetHabitSchedule() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit userHabit = dbService.insertUserHabit(habit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);

        when(habitScheduleRepository.findHabitScheduleByIdAndUserHabit_User_Id(habitSchedule.getId(), user.getId()))
                .thenReturn(Optional.of(habitSchedule));

        HabitScheduleService habitScheduleService = new HabitScheduleServiceImp(habitScheduleRepository);

        HabitSchedule result = habitScheduleService.getHabitSchedule(habitSchedule.getId(), user.getId());

        assertEquals(habitSchedule, result);
        verify(habitScheduleRepository, times(1))
                .findHabitScheduleByIdAndUserHabit_User_Id(habitSchedule.getId(), user.getId());
    }

    @Test
    @Transactional
    void testGetHabitScheduleByHabitId() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit userHabit = dbService.insertUserHabit(habit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);

        when(habitScheduleRepository.findHabitScheduleByUserHabit_Habit_IdAndUserHabit_User_Id(habitSchedule.getId(), user.getId()))
                .thenReturn(Optional.of(habitSchedule));

        HabitScheduleService habitScheduleService = new HabitScheduleServiceImp(habitScheduleRepository);

        HabitSchedule result = habitScheduleService.getHabitScheduleByHabitId(habitSchedule.getId(), user.getId());

        assertEquals(habitSchedule, result);
        verify(habitScheduleRepository, times(1))
                .findHabitScheduleByUserHabit_Habit_IdAndUserHabit_User_Id(habitSchedule.getId(), user.getId());
    }

    @Test
    @Transactional
    void testGetHabitScheduleByHabitIdWithoutUserId() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit userHabit = dbService.insertUserHabit(habit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);

        when(habitScheduleRepository.findHabitScheduleByUserHabit_Habit_Id(habit.getId()))
                .thenReturn(habitSchedule);

        HabitScheduleService habitScheduleService = new HabitScheduleServiceImp(habitScheduleRepository);

        HabitSchedule result = habitScheduleService.getHabitScheduleByHabitId(habit.getId());

        assertEquals(habitSchedule, result);
    }


    @Test
    @Transactional
    void testUpdateHabitScheduleCron() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit userHabit = dbService.insertUserHabit(habit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);

        when(habitScheduleRepository.findHabitScheduleByIdAndUserHabit_User_Id(habitSchedule.getId(), user.getId()))
                .thenReturn(Optional.of(habitSchedule));
        when(habitScheduleRepository.save(any(HabitSchedule.class))).thenReturn(habitSchedule);

        HabitScheduleService habitScheduleService = new HabitScheduleServiceImp(habitScheduleRepository);

        HabitSchedule result = habitScheduleService.updateHabitScheduleCron(habitSchedule.getId(), "0 0 8 * * 1", user.getId());

        assertEquals("0 0 8 * * 1", result.getCron());
        verify(habitScheduleRepository, times(1)).save(any(HabitSchedule.class));
        verify(habitScheduleRepository, times(1)).findHabitScheduleByIdAndUserHabit_User_Id(habitSchedule.getId(), user.getId());
    }
}
