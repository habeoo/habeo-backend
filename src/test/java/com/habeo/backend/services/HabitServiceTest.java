package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.services.HabitServiceImpl;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
public class HabitServiceTest {
    @Autowired
    private HabitServiceImpl habitService;

    @Autowired
    private DBService dbService;


    @Test
    @Transactional
    public void getHabit__Should_GetHabit_When_HabitExists() {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit h = dbService.insertHabit();

        UserHabit userHabit = dbService.insertUserHabit(h, user, tag);
        Optional<Habit> habit = habitService.getHabitById(h.getId(), user.getId());
        assertTrue(habit.isPresent());
    }

    @Test
    @Transactional
    public void createHabit__Should_ReturnHabit_When_BooleanHabitIsCreated() throws IOException {
        User user = dbService.insertUser();
        Habit result = dbService.insertBooleanHabitDto(user);

        assertNotNull(result);
    }

    @Test
    @Transactional
    public void createHabit__Should_ReturnHabit_When_MeasurableHabitIsCreated() throws IOException {
        User user = dbService.insertUser();
        Habit result = dbService.insertMeasurableHabitDto(user);

        assertNotNull(result);
    }
}
