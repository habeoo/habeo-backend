package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.exceptions.ForbiddenResourceException;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.services.UserHabitsServiceImp;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
public class UserHabitsServiceTest {

    @Autowired
    private UserHabitsServiceImp userHabitsServiceImp;

    @Autowired
    private DBService dbService;

    @Test
    @Transactional
    void saveUserHabit() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit expectedUserHabit = dbService.insertUserHabit(habit, user, tag);

        UserHabit actualUserHabit = userHabitsServiceImp.saveUserHabit(user, habit, tag);

        assertEquals(expectedUserHabit.getHabit(), actualUserHabit.getHabit());
        assertEquals(expectedUserHabit.getUser(), actualUserHabit.getUser());
        assertEquals(expectedUserHabit.getTag(), actualUserHabit.getTag());
    }


    @Test
    @Transactional
    void habitBelongsToUser_returnsTrue_whenUserHabitExists() throws IOException {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit habit = dbService.insertBooleanHabitDto(user);
        dbService.insertUserHabit(habit, user, tag);
        boolean actualResult = userHabitsServiceImp.habitBelongsToUser(user.getId(), habit);

        assertTrue(actualResult);
    }

    @Test
    @Transactional
    void habitBelongsToUser_throwsForbiddenResourceException_whenUserHabitDoesNotExist() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();

        assertThrows(ForbiddenResourceException.class, () -> {
            userHabitsServiceImp.habitBelongsToUser(user.getId(), habit);
        });
    }

    @Test
    @Transactional
    void getAllUserHabits() {
        User user = dbService.insertUser();
        Habit habit = dbService.insertHabit();
        Tag tag = dbService.insertTag(user);
        UserHabit expectedUserHabit = dbService.insertUserHabit(habit, user, tag);
        List<UserHabit> expectedUserHabitList = new ArrayList<>();
        expectedUserHabitList.add(expectedUserHabit);

        List<UserHabit> actualUserHabitList = userHabitsServiceImp.getAllUserHabits(user.getId());

        assertEquals(expectedUserHabitList, actualUserHabitList);
    }

    @Test
    @Transactional
    void getAllHabits() throws IOException {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit habit = dbService.insertMeasurableHabitDto(user);
        Habit booleanHabit = dbService.insertBooleanHabitDto(user);
        dbService.insertUserHabit(habit, user, tag);
        dbService.insertUserHabit(booleanHabit, user, tag);

        List<Habit> actualHabitList = userHabitsServiceImp.getAllHabits(user.getId());

        assertTrue(actualHabitList.contains(habit));
        assertTrue(actualHabitList.contains(booleanHabit));
    }
}
