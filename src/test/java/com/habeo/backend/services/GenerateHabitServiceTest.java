package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.exceptions.BusinessException;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.repositories.GeneratedHabitRepository;
import com.habeo.backend.habits.generated.services.GenerateHabitServiceImpl;
import com.habeo.backend.habits.generated.services.HabitEventService;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.services.UserHabitsService;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class GenerateHabitServiceTest {
    @Mock
    GeneratedHabitRepository generatedHabitRepository;


    @Autowired
    UserHabitsService userHabitsService;

    @Autowired
    GenerateHabitServiceImpl generateHabitService;

    @Autowired
    DBService dbService;

    @Autowired
    HabitEventService habitEventService;


    @Test
    @Transactional
    void updateHabitEvent__Should_ReturnGeneratedHabit_When_Saved() {
        GeneratedHabit existingHabit = dbService.insertGeneratedHabit();
        Long testId = existingHabit.getId();

        when(generatedHabitRepository.findById(testId)).thenReturn(Optional.of(existingHabit));
        GeneratedHabit editedHabit = new GeneratedHabit();
        editedHabit.setStart(OffsetDateTime.now());
        Optional<GeneratedHabit> result = generateHabitService.updateGeneratedHabit(testId, editedHabit);

        assertTrue(result.isPresent());
        assertEquals(existingHabit.getId(), result.get().getId());
        assertEquals(editedHabit.getStart(), result.get().getStart());
        assertEquals(HabitStatus.pending, result.get().getStatus());
    }

    @Test
    @Transactional
    void generateHabits__Should_ReturnListOfGeneratedHabits_When_Saved() {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit testHabit = dbService.insertHabit();

        UserHabit userHabit = dbService.insertUserHabit(testHabit, user, tag);
        HabitSchedule habitSchedule = dbService.insertHabitSchedule(userHabit);
        generateHabitService.generateHabit(habitSchedule);
        Optional<List<GeneratedHabit>> generatedHabit = habitEventService.getHabitEvents(testHabit.getId(), user.getId());
        assertNotNull(generatedHabit);
    }

    @Test
    @Transactional
    void habitBelongsToUser__Should_ReturnTrue_WhenMatch_Else_ThrowForbiddenException() {
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);
        Habit testHabit = dbService.insertHabit();
        User finalUser = dbService.insertUser();
        UserHabit userHabit = dbService.insertUserHabit(testHabit, user, tag);
        assertThrows(BusinessException.class, () -> userHabitsService.habitBelongsToUser(finalUser.getId(), userHabit.getHabit()));
        assertTrue(userHabitsService.habitBelongsToUser(user.getId(), testHabit));
    }
}
