package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.models.dtos.CreateGroupDto;
import com.habeo.backend.groups.models.dtos.UpdateGroupDto;
import com.habeo.backend.groups.repositories.GroupRepository;
import com.habeo.backend.groups.services.GroupService;
import com.habeo.backend.groups.services.GroupServiceImp;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.tags.models.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.*;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class GroupServiceTest {
    @Mock
    GroupRepository groupRepository;

    @Autowired
    DBService dbService;

    @Autowired
    GroupService groupService;

    @InjectMocks
    GroupServiceImp groupServiceImp;

    @Test
    @Transactional
    void createGroup__Should_ReturnGroup_When_SavedInfo() {
        User user = dbService.insertUser();


        CreateGroupDto groupDto = new CreateGroupDto("Group Name", "Group Description");
        Group group = dbService.insertGroup(user);
        when(groupRepository.save(any(Group.class))).thenReturn(group);

        Group result = groupServiceImp.createNewGroup(groupDto, user);

        assertNotNull(result);
        assertEquals(group.getName(), result.getName());
        assertEquals(group.getDescription(), result.getDescription());
        assertEquals(group.getMembers(), result.getMembers());
        verify(groupRepository, times(1)).save(any(Group.class));
    }

    @Test
    @Transactional
    void getUserGroups__Should_ReturnUserGroups_When_Present() {
        User user = dbService.insertUser();

        List<Group> groups = Arrays.asList(
                dbService.insertGroup(user),
                dbService.insertGroup(user)
        );
        when(groupRepository.findAllByMembersContaining(user)).thenReturn(groups);

        List<Group> result = groupServiceImp.getUserGroups(user);

        assertNotNull(result);
        assertEquals(groups.size(), result.size());
        assertEquals(groups.get(0).getName(), result.get(0).getName());
        assertEquals(groups.get(0).getDescription(), result.get(0).getDescription());
        assertEquals(groups.get(0).getInvitationUrl(), result.get(0).getInvitationUrl());
        assertEquals(groups.get(0).getMembers(), result.get(0).getMembers());
        assertEquals(groups.get(1).getName(), result.get(1).getName());
        assertEquals(groups.get(1).getDescription(), result.get(1).getDescription());
        assertEquals(groups.get(1).getInvitationUrl(), result.get(1).getInvitationUrl());
        assertEquals(groups.get(1).getMembers(), result.get(1).getMembers());
        verify(groupRepository, times(1)).findAllByMembersContaining(user);
    }

    @Test
    @Transactional
    void getUserGroups__Should_ThrowException_When_NotFound() {
        User user = dbService.insertUser();

        when(groupRepository.findAllByMembersContaining(user)).thenReturn(Collections.emptyList());

        assertThrows(NotFoundException.class, () -> groupServiceImp.getUserGroups(user));

        verify(groupRepository, times(1)).findAllByMembersContaining(user);
    }

//    @Test
//    @Transactional
//    void updateGroup__Should_ReturnGroup_When_Updated() {
//        User user = dbService.insertUser();
//
//        UpdateGroupDto groupDto = new UpdateGroupDto(
//                "Updated Group",
//                "This group has been updated."
//        );
//        Group group = dbService.insertGroup(user);
//
//        Group result = groupService.updateGroup(groupDto, group.getId(), user);
//
//        assertAll("Group",
//                () -> assertEquals(group.getId(), result.getId()),
//                () -> assertEquals(groupDto.name(), result.getName()),
//                () -> assertEquals(groupDto.description(), result.getDescription())
//        );
//    }
}
