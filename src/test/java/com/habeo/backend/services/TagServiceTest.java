package com.habeo.backend.services;

import com.habeo.backend.DBService;
import com.habeo.backend.exceptions.NotFoundException;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.oauth2.repositories.UserRepository;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.models.dtos.CreateTagDto;
import com.habeo.backend.tags.repositories.TagRepository;
import com.habeo.backend.tags.services.TagService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
public class TagServiceTest {
    @Autowired
    TagService tagService;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DBService dbService;

    @Test
    @Transactional
    public void getTag_ShouldGetTag_WhenTagExists() {
        String name = "Updated tag";
        User user = dbService.insertUser();
        Tag t = dbService.insertTag(user);
        Tag tag = tagService.getTagById(t.getId(), user.getId());

        assertEquals(name, tag.getName());
    }

    @Test
    @Transactional
    public void getTag_ShouldThrowNotFoundException_WhenAccessingForeignResource() {
        final User finalUser = dbService.insertUser();
        User user = dbService.insertUser();
        Tag tag = dbService.insertTag(user);

        assertThrows(NotFoundException.class, () -> tagService.getTagById(tag.getId(), finalUser.getId()));
    }

    @Test
    @Transactional
    public void createTag_ShouldReturnTag_WhenSavedInfo() {
        CreateTagDto createTagDto = new CreateTagDto("Test tag",  false);
        User user = dbService.insertUser();
        Tag tag = tagService.createNewTag(createTagDto, user.getId());

        assertNotNull(tag);
        assertNotNull(tag.getId());
        assertEquals(createTagDto.name(), tag.getName());
        assertEquals(user.getId(), tag.getUser().getId());
        assertEquals(createTagDto.defaultTag(), tag.getDefaultTag());
    }

    @Test
    @Transactional
    public void updateTag_ShouldReturnTag_WhenUpdated() {
        User user = dbService.insertUser();
        Tag t = dbService.insertTag(user);
        CreateTagDto updatedTag = new CreateTagDto("Updated tag", false);
        Tag tag = tagService.updateTag(t.getId(), updatedTag, user.getId());

        assertNotNull(tag);
        assertEquals(t.getId(), tag.getId());
        assertEquals(updatedTag.name(), tag.getName());
    }
}
