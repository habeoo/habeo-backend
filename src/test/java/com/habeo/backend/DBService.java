package com.habeo.backend;

import com.habeo.backend.groups.models.Group;
import com.habeo.backend.groups.repositories.GroupRepository;
import com.habeo.backend.habits.generated.models.GeneratedHabit;
import com.habeo.backend.habits.generated.models.HabitSchedule;
import com.habeo.backend.habits.generated.models.HabitStatus;
import com.habeo.backend.habits.generated.repositories.GeneratedHabitRepository;
import com.habeo.backend.habits.generated.repositories.HabitScheduleRepository;
import com.habeo.backend.habits.scheme.models.BooleanHabit;
import com.habeo.backend.habits.scheme.models.Habit;
import com.habeo.backend.habits.scheme.models.UserHabit;
import com.habeo.backend.habits.scheme.models.dtos.BooleanHabitDto;
import com.habeo.backend.habits.scheme.models.dtos.MeasurableHabitDto;
import com.habeo.backend.habits.scheme.repositories.HabitRepository;
import com.habeo.backend.habits.scheme.repositories.UserHabitsRepository;
import com.habeo.backend.habits.scheme.services.HabitService;
import com.habeo.backend.oauth2.models.AuthProvider;
import com.habeo.backend.oauth2.models.User;
import com.habeo.backend.oauth2.repositories.UserRepository;
import com.habeo.backend.tags.models.Tag;
import com.habeo.backend.tags.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

@Service
public class DBService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HabitRepository habitRepository;

    @Autowired
    private HabitService habitService;

    @Autowired
    private GeneratedHabitRepository generatedHabitRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserHabitsRepository userHabitsRepository;

    @Autowired
    private HabitScheduleRepository habitScheduleRepository;


    @Transactional
    public User insertUser() {
        User user = new User();
        user.setName("Test");
        user.setEmail("%s%d%s".formatted("test", System.nanoTime(), "@gmail.com"));
        user.setProvider(AuthProvider.google);
        user.setPhotoUrl("test_url");
        return userRepository.save(user);
    }

    @Transactional
    public Tag insertTag(User user) {
        Tag tag = new Tag();
        tag.setDefaultTag(false);
        tag.setUser(user);
        tag.setName("Updated tag");
        return tagRepository.save(tag);
    }

    @Transactional
    public HabitSchedule insertHabitSchedule(UserHabit userHabit) {
        HabitSchedule habitSchedule = new HabitSchedule();
        habitSchedule.setCron("0 0 8 * * 1");
        habitSchedule.setUserHabit(userHabit);
        return habitScheduleRepository.save(habitSchedule);
    }

    @Transactional
    public Habit insertHabit() {
        BooleanHabit testHabit = new BooleanHabit();
        testHabit.setName("test name");
        testHabit.setGoal("test goal");
        testHabit.setType("boolean");

        return habitRepository.save(testHabit);
    }

    @Transactional
    public UserHabit insertUserHabit(Habit habit, User user, Tag tag) {
        UserHabit userHabit = new UserHabit();
        userHabit.setHabit(habit);
        userHabit.setUser(user);
        userHabit.setTag(tag);
        return userHabitsRepository.save(userHabit);
    }

    @Transactional
    public Habit insertMeasurableHabitDto(User user) throws IOException {
        Tag tag = insertTag(user);
        MeasurableHabitDto habitDto = new MeasurableHabitDto();
        habitDto.setName("test name");
        habitDto.setGoal("test goal");
        habitDto.setUnit("test unit");
        habitDto.setTagId(tag.getId());
        habitDto.setTarget(111);
        habitDto.setType("boolean");
        habitDto.setCron("0 0 8 * * 1");
        return habitService.createHabit(habitDto);
    }

    @Transactional
    public Habit insertBooleanHabitDto(User user) throws IOException {
        Tag tag = insertTag(user);
        BooleanHabitDto habitDto = new BooleanHabitDto();
        habitDto.setName("test name");
        habitDto.setGoal("test goal");
        habitDto.setTagId(tag.getId());
        habitDto.setType("boolean");
        habitDto.setCron("0 0 8 * * 1");

        return habitService.createHabit(habitDto);
    }

    @Transactional
    public GeneratedHabit insertGeneratedHabit() {
        GeneratedHabit existingHabit = new GeneratedHabit();
        existingHabit.setStatus(HabitStatus.pending);
        existingHabit.setStart(OffsetDateTime.now().minusDays(1));
        return generatedHabitRepository.save(existingHabit);
    }

    public Group insertGroup(User user) {
        Group group = new Group();
        group.setName("Group Name");
        group.setDescription("Group Description");
        group.setInvitationUrl("invitation-url");
        group.setMembers(Collections.singleton(user));
        group.setHabits(new HashSet<>());
        return groupRepository.save(group);
    }
}
